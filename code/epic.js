"use strict";
let EpicApplication = {
    retrievedData: [],
    typeValue: undefined,
    dateCache: {},
    imageCache: {}
};
let retrievedDate = [];
let latestArr = [];
function setup(event) {
    let types = document.querySelectorAll("option");
    for(let i = 0; i < types.length; ++ i) {
        EpicApplication.imageCache[types[i].value] = new Map();
        latestArr.length = 0;
        let URL = "https://epic.gsfc.nasa.gov/api/" + types[i].value + '/all';
        fetch(URL)
            .then((response) => {
                if(!response.ok) {
                    throw new Error("Fetch failed", {cause: response});
                }
                return response.json();
            })
            .then((data) => {
                retrievedDate = data;
                for(let i of retrievedDate) {
                    latestArr.push(i.date);
                }
                latestArr.sort();
                EpicApplication.dateCache[types[i].value] = latestArr[latestArr.length - 1];
            })
            .catch((error) => {
                alert(`Error: ${error.message}`);
                EpicApplication.dateCache[types[i].value] = undefined;
            });
    }
    const btn = document.querySelector("button");
    const dateList = document.getElementById("image-menu");
    const typeField = document.getElementById("type");
    let dateValue;
    function constructURL() {
        EpicApplication.typeValue = typeField.value;
        dateValue = document.getElementById("date").value;
        let URL = "https://epic.gsfc.nasa.gov/api/";
        URL += EpicApplication.typeValue;
        if(dateValue.length > 0) {
            URL += "/date/" + dateValue;
        }
        return URL;
    }

    function createList(dateArr) {
        dateList.textContent = undefined;
        dateArr.sort();
        for(let i = 0; i < dateArr.length; ++i) {
            let element = document.createElement("li");
            element.textContent = dateArr[i];
            element.setAttribute("data-imageIndex", i);
            dateList.appendChild(element);
        }
    }

    function fetchDate(e) {
        let URL = constructURL();
        EpicApplication.retrievedData.length = 0;;
        if(EpicApplication.imageCache[EpicApplication.typeValue].get(dateValue) == undefined) {
            let dateArr = [];
            fetch(URL)
                .then((response) => {
                    if(!response.ok) {
                        throw new Error("Fetch failed", {cause: response});
                    }
                    return response.json();
                })
                .then((data) => {
                    EpicApplication.retrievedData = data;
                    EpicApplication.retrievedData.forEach(element => {
                        dateArr.push(element.date);
                    });
                    EpicApplication.imageCache[EpicApplication.typeValue].set(dateValue, dateArr);
                    createList(dateArr);
                })
                .catch((error) => {
                    alert(`Error: ${error.message}`);
                });
        }else {
            createList(EpicApplication.imageCache[EpicApplication.typeValue].get(dateValue));
        }
        
        
    }

    function loadImage(e) {
        const image = document.getElementById("earth-image");
        const date = document.getElementById("earth-image-date");
        const title = document.getElementById("earth-image-title");
        let dateSplit = dateValue.split("-");
        let indexObj = EpicApplication.retrievedData[e.target.dataset.imageindex];
        let imageName = indexObj.image;
        let source = "https://epic.gsfc.nasa.gov/archive/" + 
            EpicApplication.typeValue + "/" + dateSplit[0] + "/" + dateSplit[1] + "/" + dateSplit[2] + "/jpg/" + imageName + ".jpg";
        image.src = source;
        image.alt = indexObj.caption;
        date.textContent = indexObj.date;
        title.textContent = indexObj.caption;
    }

    function loadMessage(e) {
        if(dateValue == undefined || dateValue.length == 0) {
            EpicApplication.typeValue = typeField.value;
            dateList.textContent = undefined;
            let li = document.createElement("li");
            if(EpicApplication.dateCache[EpicApplication.typeValue] == null) {
                let URL = constructURL() + '/all';
                fetch(URL)
                    .then((response) => {
                        if(!response.ok) {
                            throw new Error("Fetch failed", {cause: response});
                        }
                        return response.json();
                    })
                    .then((data) => {
                        retrievedDate = data;
                        for(let i of retrievedDate) {
                            latestArr.push(i.date);
                        }
                        latestArr.sort();
                        EpicApplication.dateCache[types[i].value] = latestArr[latestArr.length - 1];
                    })
                    .catch((error) => {
                        alert(`Error: ${error.message}`);
                        EpicApplication.dateCache[types[i].value] = undefined;
                    });
            }
            li.textContent = `The latest picture of this type is taken on ${EpicApplication.dateCache[EpicApplication.typeValue]}.`;
                dateList.appendChild(li);
        }
    }
    btn.addEventListener("click", fetchDate);
    dateList.addEventListener("click", loadImage);
    typeField.addEventListener("change", loadMessage)
}

document.addEventListener("DOMContentLoaded", setup);
